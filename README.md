### Zenith
---
This is an addon for Deathstrider that adds a hi-capacity, upgradeable, 10mm pistol. It has a chance to spawn on pistol spawns i.e. the same spawns as the Viper and Majestic. The ammunition is from [DAX](https://gitlab.com/icarus-innovations/deathstrider/dax) and requires it to be loaded before this addon.

### Credits and Permissions
---
All the cool people who inspired or helped me make this mod:
- Eidos-Montréal and Square Enix Europe for creating the original Zenith pistol used in Deus Ex: Human Revolution which this addon is based on.
- Accensus, coding and gauntlet sprites
- Icarus, all of the weapon sprites. (Yes, I'm crediting myself. Yes, I have a massive ego.)

All the assets used in this mod are original or used with permission from the original creator, if you want to use any of the assets in this mod in your own content you must get permission from the orginal creator.